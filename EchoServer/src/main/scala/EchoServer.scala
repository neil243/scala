import java.net._
import java.io._

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}

object EchoServer {
  def props: Props = Props[EchoServer]
  final case class MySocket(socket: Socket)
}

class EchoServer extends Actor {
  import EchoServer._

  def read_and_write(in: BufferedReader, out:BufferedWriter): Unit = {

    //**************
    val request = in.readLine().split(" ")
      val command = request(0)
      val file_name = "./" + request(1) + ".html"
      val file = new File(file_name)
      val version = request(2)
      if(!file.exists())
      {
        out.write("HTTP 404\r\n")
        out.write("File not found")
      }
      else
      {
        out.write("\r\n" + version + " 200 OK\r\n") //version & status code
        out.write("Content-Type: text/plain\r\n") // type of data
        out.write("Host: localhost:9999\r\n") //Host
        out.write("Connection: close\r\n") //will close stream
        out.write("\r\n") //end of headers
        out.write(scala.io.Source.fromFile(file_name).mkString)
      }

    //**************
    out.flush()
    in.close()
    out.close()
  }

  def receive: PartialFunction[Any, Unit] = {
    case MySocket(s) =>
      val in = new BufferedReader(new InputStreamReader(s.getInputStream))
      val out = new BufferedWriter(new OutputStreamWriter(s.getOutputStream))

      read_and_write(in, out)

      s.close()
  }
}

object AkkaEchoServer {
  import EchoServer._

  val system: ActorSystem = ActorSystem("AkkaEchoServer")
  val my_server: ActorRef = system.actorOf(EchoServer.props)

  def main(args: Array[String]) {
    val server = new ServerSocket(9999)
    while(true) {
      val s = server.accept()
      my_server ! MySocket(s)
    }
  }
}